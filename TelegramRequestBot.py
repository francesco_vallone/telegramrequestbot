import flask
import botogram

app = flask.Flask(__name__)
chan = botogram.channel("YOUR-CHANNEL-ID", "YOUR-BOT-KEY")


@app.route('/request', methods=['GET', 'POST'])
def request():
    if flask.request.method == 'POST' and \
            'name' in flask.request.form and 'request' in flask.request.form and 'email' in flask.request.form \
            and 'telegram' in flask.request.form:
        name = flask.request.form['name']
        request = flask.request.form['request']
        email = flask.request.form['email']
        telegram = flask.request.form['telegram']
        chan.send(
            "*The request*: " + request + "\n*has been sent by*: " + name + "\n*Contact him from*: " + email + "\n" + telegram)
        return flask.redirect(flask.url_for('successful'))
    return flask.render_template('request.html')


@app.route("/successful")
def successful():
    return flask.render_template('successful.html')


if __name__ == '__main__':
    app.run()
    chan.run()